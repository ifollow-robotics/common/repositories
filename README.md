# iFollow repositories

Go through the following steps to be able to install the packages

```bash
#necessary for apt-get to support https
sudo apt-get install apt-transport-https
```

Modify sources

```bash
sudo /bin/bash -c "echo 'deb [trusted=yes] https://gitlab.com/ifollow-robotics/common/repositories/raw/master xenial main' >> /etc/apt/sources.list"
```

Update! Ignore the errors you face

```bash
sudo apt-get update
```

Install the packages

```bash
sudo apt-get install linux-image-4.16.18-rt12 linux-headers-4.16.18-rt12 jlink
```

## Adding a repository from a .deb file

Clone this project, download the .deb, use reprepro to add it: https://www.digitalocean.com/community/tutorials/how-to-use-reprepro-for-a-secure-package-repository-on-ubuntu-14-04